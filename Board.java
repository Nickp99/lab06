public class Board{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles= new boolean[12];
	}
	public String toString(){
		String board ="";
		for(int i=0;i<tiles.length;i++){
			if(tiles[i]== false){
				int a=i+1;
				board+= " "+a;
			}
			else{
				board+=" X";
			}
		}
		return board;
	}
	public boolean PlayATurn(){
		this.die1.roll();
		this.die2.roll();
		int sumOfDie= this.die1.GetFaceValue() + this.die2.GetFaceValue();
		int arraylength= sumOfDie -1;
		int arrayLengthDie1 = this.die1.GetFaceValue() -1;
		int arrayLenghtDie2 = this.die2.GetFaceValue() -1;
		
		
		if(tiles[arraylength] == false ){
			tiles[arraylength] = true;
			System.out.println("Closing tile equal to sum: "+ sumOfDie);
			return false;
		}
		
		else if(tiles[arrayLengthDie1] == false){
			tiles[arrayLengthDie1] = true;
			System.out.println("Closing tile with the same value as die one: "+ this.die1);
			return false;
		}
		
		else if (tiles[arrayLenghtDie2]  == false){
			tiles[arrayLenghtDie2] = true;
			System.out.println("Closing tile with the same value as die two: "+ this.die1);
			return false;
		}
		
		else {
			return true;
		}
	}
}