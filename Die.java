import java.util.Random;
public class Die{
	private int faceValue;
	private Random randomNum;

	public Die(){
		this.faceValue=1;
		this.randomNum = new Random();
	}		
	public int GetFaceValue(){
		return this.faceValue;
	}
	public void roll(){
		this.faceValue = 1 + randomNum.nextInt(6);
		
	}
	public String toString(){
		return "You rolled a "+ this.faceValue;
	}
}